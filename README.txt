CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * How To Use
 * Contributors

INTRODUCTION
------------

*** You do not need these module unless you're either instructed by another module to install this or you're a theme or
module developer ***

The problem with Drupal theming is that in most cases we have to write at least few lines of code. And it's occurring
more often because of new complicated modern cross browsers html5 templates :). If we need to for example remove some
third party modules' stylesheets or javascripts due to the conflict with the theme styles/scripts, there is no way other
then writing code. So theming in Drupal has become a tedious task specially for non programmers or even for programmers
with no familiarities with Drupal API. No wonder we have so few high quality Drupal themes :(

The purpose of this module is to eliminate this complexity once and for all, at least for styles and scripts. To achieve
this .info is extended by the module and several new parameters are added which gives us the power to have total control
over styles and scripts. So lets have a look at the supported features :

- Full support for drupal_add_css and drupal_add_js parameters and even more!
-- Adding external files
-- Defining where to include it : header / footer
-- Adding inline css/js
-- Whether to display on all pages or not
-- Defining style/script group : theme / module / core
-- Weight (the order of adding to the page)
-- Supporting Defer parameter
-- Enable/Disable caching per style/script
-- Enable/Disable preprocessing
-- Enable/Disable using core
-- Adding attributes like id to stylesheet/javascript include tags
-- Support for IE conditional comments for both styles and scripts
-- Defining style media : print/all/screen
- Manipulating existing styles/scripts
-- Creating a whitelist or blacklist to decide which style/scripts should be added to the page
-- Possibility of replacing and overriding core and contributed modules styles and scripts using only the info file
- Possibility of altering the scripts and styles (hook_js_alter and hook_css_alter support for Drupal 6)
- Compatible with most of the style and a script manipulation modules
- Adds theme_path variable to be used by template files and css_js_total_control_get_theme_path function


INSTALLATION
------------


HOW TO USE
------------

The module does not have any administration page and all settings should be done in .info file.

The module introduces 4 new parameters to .info files :
- stylesheets-settings and scripts-settings : Altering existing styles/scripts, even the ones belong to core or
  contributed modules. You can for example remove all contributed module scripts or replace core jquery module with
  an newer version
  Note that options are all the same for both stylesheets-settings and scripts-settings
  It's easier to show how it works by showing some examples

  ;Allowing only necessary stylesheets
  stylesheets-settings[filter][rules][0][function] = regular_expression
  stylesheets-settings[filter][rules][0][pattern] = %settings|admin|misc|jquery_update%
  stylesheets-settings[filter][type] = whitelist

  ;Removing some scripts based on some criteria in path or type
  scripts-settings[filter][rules][0][function] = regular_expression
  scripts-settings[filter][rules][0][pattern] = %misc/jquery|jquery_update|textarea|collapse|tiny_mce|modules%
  scripts-settings[filter][rules][1][function] = regular_expression
  scripts-settings[filter][rules][1][pattern] = %setting|inline%
  scripts-settings[filter][rules][1][field] = type
  scripts-settings[filter][type] = blacklist


- stylesheets-extended, scripts-extended : Adding new style/scripts to the page, you can do almost anything you want,
  from adding a simple stylesheet to adding an inline script at the bottom of the page. You can even add attributes to
  style/script include tag.
  Note that options are all the same for both stylesheets-extended and scripts-extended
  Here are some examples :

  ;Simple usage.
  stylesheets-extended[css/bootstrap.min.css][media] = all

  ;Adding an style only if IE 7
  stylesheets-extended[css/font-awesome-ie7.min.css][condition-string] = if IE 7

  ;Adding an id to a stylesheet include html tag
  stylesheets-extended[css/menu/styles/lblack.css][media] = all
  stylesheets-extended[css/menu/styles/skins/lblack.css][attributes][id] = custom_menu

  ;Adding external stylesheet
  stylesheets-extended[http://example.com/mystyle.css][media] = all

  ;Adding an script at the bottom of the html
  scripts-extended[js/vendor/selectivizr.min.js][scope] = footer

  ;Moving an script before all the other scripts. To achieve this group should also be changed to core.
  ;otherwise it will only appear before theme scripts. If you've removed core jquery using scripts-settings
  ;by the following lines you've successfully replaced core jquery
  scripts-extended[js/vendor/jquery.min.js][scope] = header
  scripts-extended[js/vendor/jquery.min.js][weight] = 0
  scripts-extended[js/vendor/jquery.min.js][group] = core

  ;Adding an inline script at the bottom of the html. Note that module reads the menu-effect.inline.js file
  ;and print it content as inline script on page
  scripts-extended[js/menu-effect.inline.js][scope] = footer
  scripts-extended[js/menu-effect.inline.js][type] = inline

  ;At the default version of easing library. (Relied on libraries module's API to load it)
  scripts-extended[easing][type] = library
  scripts-extended[easing][version] = default

  ;At some settings to Drupal js variable
  scripts-extended[mytheme][type] = setting
  scripts-extended[mytheme][setting][name] = special

*** IMPORTANT ***

Drupal 6 also stores a cache of the data in .info files. If you modify any lines
in your theme's .info file, you MUST refresh Drupal 6's cache by simply visiting
the admin/build/themes page.

CONTRIBUTORS
------------
 Current Maintainer: Sina Salek <sina.salek.ws>
