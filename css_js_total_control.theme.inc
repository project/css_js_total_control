<?php
/**
 * @file
 * Extends .info file to support more features
 */

/**
 * Return paths for the theme and its base themes.
 *
 * @param $theme
 *   The name of the theme.
 * @return
 *   An array of all the theme paths.
 */
function css_js_total_control_paths_to_basetheme($theme) {
  static $theme_paths;
  if (empty($theme_paths[$theme])) {
    $theme_paths[$theme] = array();
    $themes = list_themes();
    // Grab the paths from the base theme.
    if (!empty($themes[$theme]->base_theme)) {
      $theme_paths[$theme] = css_js_total_control_paths_to_basetheme($themes[$theme]->base_theme);
    }
    $theme_paths[$theme][$theme] = dirname($themes[$theme]->filename);
  }
  return $theme_paths[$theme];
}

/**
 * Alter the information parsed from module and theme .info files
 * Adding default values for options of additional parameters added to info file by this module.
 * So users need only to override options as needed
 *
 * @param $info
 * @param $file
 */
function _css_js_total_control_system_info_alter(&$info, $file) {
  if (!isset($info['stylesheets-extended'])) {
    $info['stylesheets-extended'] = array();
  }
  if (!isset($info['scripts-extended'])) {
    $info['scripts-extended'] = array();
  }
  if (!empty($info['stylesheets-extended']) || !empty($info['scripts-extended'])) {
    $file_info_default = array(
      'scope' => 'header',
      'media' => 'all',
      'external' => FALSE,
      'preprocess' => TRUE,
      'every_page' => TRUE,
      'type' => 'file',
      'group' => 'theme',
      'condition-string' => NULL,
      'browsers' => array(),
      'use_core' => FALSE,
      'attributes' => NULL,
      'weight' => NULL,
      
      //Not supported, only to prevent PHP NOTICE
      'cache' => TRUE,
      'defer' => FALSE
    );
    foreach ($info['stylesheets-extended'] as $file => $file_info) {
      $file_info = array_merge($file_info_default, $file_info);
      if (preg_match('%http:\/\/%si', $file)) {
        $file_info['external'] = TRUE;
      }
      if (!isset($file_info['media']) || empty($file_info['media'])) {
        $file_info['media'] = 'all';
      }
      //if (!$file_info['condition-string'] && !$file_info['external'] && !$file_info['attributes']) {
        //$info['stylesheets'][$file_info['media']][] = $file;
        //unset($info['stylesheets-extended'][$file]);
      //} else {
      $info['stylesheets-extended'][$file] = $file_info;
      //}
    }

    $file_info_default = array(
      'scope' => 'header',
      'external' => FALSE,
      'every_page' => TRUE,
      'type' => 'file',
      'group' => 'theme',
      'weight' => NULL,
      'condition-string' => NULL,
      'browsers' => array(),
      'defer' => FALSE,
      'cache' => TRUE,
      'preprocess' => TRUE,
      'use_core' => FALSE,
      'attributes' => NULL
    );
    
    foreach ($info['scripts-extended'] as $file => $file_info) {
      $file_info = array_merge($file_info_default, $file_info);
      if (preg_match('%http:\/\/%si', $file)) {
        $file_info['external'] = TRUE;
      }
      
      if (!$file_info['condition-string'] && !$file_info['external'] && !$file_info['attributes']) {
        $file_info['use_core'] = TRUE;
      }
      
      $info['scripts-extended'][$file] = $file_info;
    }
  }
  
}

/**
 * When the theme registry is rebuilt, we also build the module additional parameters.
 * It parses and process styles/scripts and applies the changes, it also caches the result to improve performance when
 * possible.
 *
 * @param $existing
 * @param $type
 * @param $theme
 * @param $path
 * @return array
 */
function _css_js_total_control_theme($existing, $type, $theme, $path) {
  
  //--(Begin)--> Module should be the module to run
  if (db_is_active()) {
    // Find out the weight of the other module
    $weight = db_result(db_query("SELECT weight FROM {system} ORDER BY weight DESC"));

    // Set our module to a weight 1 higher
    db_query("UPDATE {system} SET weight = %d WHERE name = 'css_js_total_control'", $weight + 1);
  }
  //--(End)--> Module should be the module to run
  
  // Process the styles/scripts for every active theme.
  $themes = list_themes();

  foreach (array_keys($themes) AS $theme) {
    // We only need to process active themes.
    if ($themes[$theme]->status || $GLOBALS['theme'] == $theme) {
      $paths = css_js_total_control_paths_to_basetheme($theme);
      
      // Grab all the styles/scripts.
      $files = array();
      $files_index = array();
      // Start with the base theme and travel up the chain to the active theme.
      foreach ($paths AS $theme_name => $path) {
        // Look at the module parameters defined in the theme's .info file.
        if (!empty($themes[$theme_name]->info['stylesheets-extended'])) {
          foreach ($themes[$theme_name]->info['stylesheets-extended'] AS $file => $file_info) {
            // Allow the theme to override its base themes' styles.
            $file_info['path'] = $path;
            $file_info['file_type'] = 'stylesheet';
            $files[$file] = $file_info;
          }
        }
        
        if (!empty($themes[$theme_name]->info['scripts-extended'])) {
          foreach ($themes[$theme_name]->info['scripts-extended'] AS $file => $file_info) {
            // Allow the theme to override its base themes' styles.
            $file_info['path'] = $path;
            $file_info['file_type'] = 'javascript';
            $files[$file] = $file_info;
          }
        }
      }

      $html_tags = $html_tags_rtl = array();

      if (!empty($files)) {
        
        $preprocess_css = (variable_get('preprocess_css', FALSE) && (!defined('MAINTENANCE_MODE') || MAINTENANCE_MODE != 'update'));
        $preprocess_js = (variable_get('preprocess_js', FALSE) && (!defined('MAINTENANCE_MODE') || MAINTENANCE_MODE != 'update'));
        //$preprocess_css = FALSE;
        //$preprocess_js = FALSE;

        $directory = file_directory_path();
        $is_writable = is_dir($directory) && is_writable($directory) && (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PUBLIC);

        // Sort files by their group when preprocess is enabled
        if (($preprocess_css || $preprocess_js) && $is_writable) {
          $groups_files = array();
          foreach ($files as $file => $file_info) {
            $group = 'all';
            if (!$file_info['external'] && !empty($file_info['condition-string']) && $file_info['preprocess']) {
              $group = $file_info['condition-string'];
            }
            if ((!$preprocess_js && $file_info['file_type'] == 'javascript') || 
              (!$preprocess_css && $file_info['file_type'] == 'stylesheet') ||
              $file_info['type'] == 'inline' ||
              $file_info['attributes']) {
              $group = 'all';
            }
            if ($file_info['use_core']) {
              continue;
            }
            $file_info['group_type'] = $group;
            $file_info['group_name'] = "{$file_info['file_type']}:{$file_info['scope']}:{$file_info['cache']}:{$file_info['defer']}:{$group}";
            $groups_files[$file_info['group_name']][$file] = $file_info;
          }
          $files = array();
          foreach ($groups_files as $group_files) {
            $files = array_merge($files, $group_files);
          }
        }

        // Make the html tags
        $group_counter = array();
        $grouped_files_for_preprocess = array();

        foreach ($files as $file => $file_info) {
          // Count the number of encountered files from each group
          if (isset($file_info['group_name'])) {
            if (!isset($group_counter[$file_info['group_name']])) {
              $group_counter[$file_info['group_name']] = 0;
            }
            $group_counter[$file_info['group_name']]++;
          }
          
          $query_string = '?'. substr(variable_get('css_js_query_string', '0'), 0, 1);
          if (!$file_info['cache']) {
            $query_string = '?' . time();
          }
          
          $path = $file_info['path'];
          // Each condition requires its own set of links.
          $html_tags_temp = '';
          $html_tags_rtl_temp = '';
          $file_address = NULL;
          $file_address_url = NULL;
          if ($file_info['external'] || file_exists("./$path/$file")) {
            $file_address = $file;
            if (!$file_info['external']) {
              $file_address = "/$path/$file";
              $file_address_url = str_replace('//', '/', base_path() . $file_address);
            } else {
              $query_string = '';
            }
          }
          
          if ($file_address) {
            $grouped_files_for_preprocess[$file_info['group_name']][$file_address] = $file_info;
            
            $file_address_rtl = NULL;
            if (!$file_info['external'] && $file_info['file_type'] == 'stylesheet') {
              $file_address_rtl = '.' . str_replace('.css', '-rtl.css', $file_address);
              if (file_exists($file_address_rtl)) {
                $grouped_files_for_preprocess[$file_info['group_name']][$file_address_rtl] = $file_info;
              } else {
                $file_address_rtl = NULL;
              }
            }
            
            if (isset($file_info['group_name']) && $file_info['group_type'] != 'all') {
              $file_address = NULL;
              if (count($groups_files[$file_info['group_name']]) == $group_counter[$file_info['group_name']]) {
                if ($file_info['file_type'] == 'stylesheet')  {
                  $filename = 'css_' . md5($file_info['group_name'] . $query_string) . '.css';
                  $file_address_url = drupal_build_css_cache(array($file_info['group_name'] => $grouped_files_for_preprocess[$file_info['group_name']]), $filename);
                  $file_address_url_rtl = NULL;
                }
                
                if ($file_info['file_type'] == 'javascript')  {
                  $filename = 'css_' . md5($file_info['group_name'] . $query_string) . '.css';
                  $file_address_url = drupal_build_js_cache($grouped_files_for_preprocess[$file_info['group_name']], $filename);
                }
              }
            }
            
            if (!empty($file_address)) {
              if ($file_info['type'] == 'inline') {
                $file_info['source'] = file_get_contents('.' . $file_address);
              }
              
              //$file_address = url($file_address);
              $attributes_html = NULL;
              if (isset($file_info['attributes'])) {
                $attributes_html = drupal_attributes($file_info['attributes']);
              }
              if ($file_info['file_type'] == 'stylesheet') {
                if ($file_info['type'] == 'inline') {
                  $link = "<style type=\"text/css\" media=\"{$file_info['media']}\" {$attributes_html}>\n{$file_info['source']}\n</style>\n";
                } else {
                  $link = "<link type=\"text/css\" rel=\"stylesheet\" media=\"{$file_info['media']}\" {$attributes_html} href=\"$file_address_url$query_string\" />\n";
                }
                
              } else {
                if ($file_info['defer']) {
                  $defer = 'defer="defer"';
                } else {
                  $defer = '';
                }
                
                if ($file_info['type'] == 'inline') {
                  $link = "<script type=\"text/javascript\" {$attributes_html} >/* <![CDATA[ */\n{$file_info['source']}\n/* ]]> */\n</script>\n";
                } else {
                  $link = "<link type=\"text/javascript\" {$attributes_html} {$defer} src=\"$file_address_url$query_string\" />\n";
                }
              }
            
              $html_tags_temp .= $link;
              
              $files_index[$file_address] = $file_info;
            }
                   
            if (!empty($file_address_rtl)) {
              $file_address_rtl_url = str_replace('//', '/', base_path() . $file_address_rtl);
              
              if ($file_info['type'] == 'inline') {
                $file_info['source_rtl'] = file_get_contents('.' . $file_address_rtl);
                $html_tags_rtl_temp = "<style type=\"text/css\" rel=\"stylesheet\" media=\"{$file_info['media']}\">/* <![CDATA[ */\n{$file_info['source_rtl']}\n/* ]]> */\n</style>\n";
              } else {
                $html_tags_rtl_temp .= "<link type=\"text/css\" rel=\"stylesheet\" media=\"{$file_info['media']}\" href=\"$file_address_rtl_url$query_string\" />\n";
              }
            }
          }
          
          $files[$file] = $file_info;
          if ($file_info['use_core']) {
            continue;
          }
          
          if (!empty($html_tags_temp) && !empty($file_address)) {
            if ($file_info['condition-string']) {
              $html_tags_temp = "<!--[{$file_info['condition-string']}]>\n$html_tags_temp<![endif]-->\n";
              $html_tags_rtl_temp = "<!--[{$file_info['condition-string']}]>\n$html_tags_rtl_temp<![endif]-->\n";
            }
            $html_tags[$file_info['file_type']][$file_info['scope']]['source'] .= $html_tags_temp;
            if (!empty($file_address_rtl)) {
              $html_tags_rtl[$file_info['file_type']][$file_info['scope']]['source_rtl'] .= $html_tags_rtl_temp;
            }
          }
        }
      }

      //echo "\n$theme\n";
      //print_r($html_tags);
      //print_r($html_tags_rtl);
      // Save the stylesheets for later retrieval.
      foreach (array('stylesheet', 'javascript') AS $type) {
        foreach (array('header', 'footer') AS $scope) {
          $var_name = 'css_js_total_control_' . $theme . '_' . $type . '_' . $scope;
          $html_tags_val = NULL;
          if (isset($html_tags[$type]) && isset($html_tags[$type][$scope])) {
            $html_tags_val = $html_tags[$type][$scope];
          }
          
          if (db_is_active()) {
            if (!empty($html_tags_val)) {
              variable_set($var_name, $html_tags_val['source']);
              variable_set($var_name . '_rtl', $html_tags_val['source_rtl']);
            } else {
              variable_del($var_name);
              variable_del($var_name . '_rtl');
            }
          }
          else {
            $GLOBALS['conf'][$var_name] = $html_tags_val['source'];
            $GLOBALS['conf'][$var_name . '_rtl'] = $html_tags_val['source_rtl'];
          }
        }
      }
      
      $var_name = 'css_js_total_control_' . $theme . '_info';
      $themes[$theme_name]->info['files'] = $files;
      $themes[$theme_name]->info['files_index'] = $files_index;
      variable_set($var_name, serialize($themes[$theme_name]->info));
    }  
  }

  // Return nothing.
  return array();
}
